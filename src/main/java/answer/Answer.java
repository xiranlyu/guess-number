package answer;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Answer {
    
    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;
    
    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            this.setAnswer(generateRandomAnswer());
        }
    }
    
    public List<Integer> getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(List<Integer> answer){
        this.answer = answer;
    }
    
    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }
    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(String.valueOf(filePath));
        BufferedReader reader = new BufferedReader(new InputStreamReader(Objects.requireNonNull(inputStream)));
        String number = reader.readLine();
        return number.chars().mapToObj(Character::getNumericValue).collect(Collectors.toList());
    }

    private List<Integer> generateRandomAnswer() {
        return new Random().ints(1, ANSWER_NUMBER_LIMIT)
                .boxed().distinct().limit(ANSWER_LENGTH).collect(Collectors.toList());
    }

    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        if (answer.stream().distinct().count() != ANSWER_LENGTH) {
            throw new InvalidAnswerException("Wrong input");
        }
    }
}
