package game;

import javafx.scene.control.Tab;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;
    
    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }
    
    
    public String getResult() {
        StringBuilder result = new StringBuilder();
        guessRecord.entrySet().stream().map(x -> result.append(x.getKey()).append(" ").append(x.getValue()).append("\n")).collect(Collectors.toList());
        return result.toString().trim();
    }
    
    public GameResult getGameResult() {
        if (guessRecord.size() >= CHANCE_LIMIT && !guessRecord.containsValue("4A0B")){
            return GameResult.LOST;
        } else if (guessRecord.size() <= CHANCE_LIMIT && guessRecord.containsValue("4A0B")) {
            return GameResult.WIN;
        } else {
            return GameResult.NORMAL;
        }
    }
}
