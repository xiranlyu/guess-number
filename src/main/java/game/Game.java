package game;

import answer.Answer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Game {
    
    private Answer answer;
    private Map<String, String> guessResult;
    
    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }
    
    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }
    
    public boolean isOver() {
        return guessResult.size() == 6 || (guessResult.containsValue("4A0B"));
    }
    
    public String getResult() {
        String answer = this.answer.getAnswer().stream().map(String::valueOf).collect(Collectors.joining(""));
        StringBuilder result = new StringBuilder();
        guessResult.entrySet().stream().map(x -> result.append(x.getKey()).append(" ").append(x.getValue()).append("\n")).collect(Collectors.toList());
        if (isOver()) {
            return guessResult.containsValue("4A0B") ?
                    result + "Congratulations, you win!"
                    : result + "Unfortunately, you have no chance, the answer is " + answer + "!";
        } else {
            return result.toString();
        }
    }
    
    private int getBSize(List<Integer> numbers) {
        return (int) numbers.stream()
                .filter(x -> answer.getAnswer().contains(x) && numbers.indexOf(x) != answer.getAnswer().indexOf(x))
                .count();
    }
    
    private int getASize(List<Integer> numbers) {
        return (int) numbers.stream()
                .filter(x -> answer.getAnswer().contains(x) && numbers.indexOf(x) == answer.getAnswer().indexOf(x))
                .count();
    }
}
